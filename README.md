# mobster_guide

A few things I have learned about how to run MOBSTER!

**MOBSTER setup**


In EDDIE:


Run `conda env create -f mobsterEnv.yml` to create environment.


Run `conda activate mobster` to open environment.


Open R within the MOBSTER environment and run _install_mobster.R_ script.


MOBSTER should now be installed to the environment. This step does NOT need to be repeated when using the MOBSTER environment in future.



**File requirements**


You will need the following files to run MOBSTER successfully:

- unzipped VCF files for all samples you would like to run
- a text file of cellularity estimates for all samples in run
- a text file with the WGD status for all samples in run
- the Combined_purple_ids_tidy.bed file (MUST be the tidy version or MOBSTER will throw a hissy fit!)
- the genelist.txt file
- MAFS for all samples in the run



**How to run MOBSTER**


Please read through _mobster.Rmd_ in GitLab. This gives step-by-step instructions on how to format your data and run MOBSTER.


Please note: the _mobster.R_ script contains exactly the same code as _mobster.Rmd_, but is in a format that can be more easily edited for use in EDDIE.


The _mobster.Rmd_ script can also be run locally with a single sample for testing purposes.
