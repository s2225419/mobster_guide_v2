---
title: "mobster"
output: html_notebook
---

This script formats the VCF files to sample level, adding cellularity and CN data. Key/driver gene names are added to the file and the cancer cell fraction (CCF) of each variant is calculated. CCF data are used to fit a MOBSTER model to the sample and the number of clusters per sample are counted.

### Load packages

```{r}

library(tidyverse)
library(here)
library(magrittr)
library(ggpubr)
library(sqldf)
library(maftools)
library(mobster)
library(vcfR)

```



### Supply sample name

This section is included for ease of transition to batch script format.

```{r}

# args <- commandArgs(trailingOnly = TRUE)
# 
# sample_name <- as.character(args[1])
# 
# vcf_file <- as.character(args[2])
# 
# maf_file <- as.character(args[3])

```

This section is included for local testing of single samples (remove if using as batch script). 

```{r}

sample_name <- "AOCS_130"

vcf_file <- "AOCS_130-ensemble-annotated-oxog_filtered.vcf"

maf_file <- "AOCS_130-ensemble-annotated-oxog_filtered.maf"

```



### Load VCF data 

# Read in VCF with *read_vcfR*

The VCF file is read in with the *vcfR::read_vcfR* function. This is used in place of the *mobster::load_vcf* function which causes errors by misaligning variant positions/VAF in the output.

```{r}

sample <- read.vcfR(here::here("path/to/HGSOC_cohort_data/vcf_unzipped", vcf_file), 
                          verbose = FALSE) %>% 
  vcfR2tidy(.,
  info_only = TRUE,
  single_frame = FALSE,
  toss_INFO_column = TRUE)

```

# Filter to tumour variants only

The file is filtered to variants from the tumour sample only and the 'AF' columnm transformed from character to numeric.

```{r}

sample <- sample$fix 

sample <- sample %>% 
  dplyr::filter(grepl(sample_name, SAMPLE))

sample$AF <- as.numeric(sample$AF)

```



### Add cellularity and copy number data

I will add all necessary data (cellularity and copy number) to the tumour sample file, which will allow me to filter by diploid variants (CN = 2) later in the analysis.  

# Generate data frame of cellularity estimates to add into each tumour sample

The cellularity data is read in with *read.table* and the sample names tidied to allow for accurate joining of the data to the sample file.

```{r}

cellularity <- read.table(here::here("data", "HGSOC_cellularity_estimates.txt"), header = FALSE, sep = "", dec = ".") %>% 
  dplyr::rename(sample = V1, cellularity_estimate = V2) 

cellularity$sample <- cellularity$sample %>% str_remove_all("T")

cellularity$sample <- cellularity$sample %>% str_remove_all("_2")

cellularity <- cellularity %>% pivot_wider(names_from = sample, values_from = cellularity_estimate) 

```

# Add cellularity values to the data  

A blank column for cellularity data is added to the sample file. Cellularity data is then added with the ifelse function (required due to idiosyncratic naming of the 'WGS' samples!)

```{r}

sample <- sample %>% 
  add_column(CELLULARITY = NA) %>% 
  relocate(CELLULARITY, .after = CHROM)

```

```{r}

if (startsWith(sample_name, "WGS")) {
sample$CELLULARITY <- cellularity %>%
  dplyr::select(matches(paste0(sample_name, "-P"))) %>%
  pull(paste0(sample_name, "-P"))
} else {
sample$CELLULARITY <- cellularity %>%
  dplyr::select(sample_name) %>%
  pull(sample_name)
}

```

# Identify sample WGD status for output labelling

I will read in WGD status here, as I want to be able to easily identify whether a sample has WGD present or absent in the output title.

```{r}

wgd <- read.table(here::here("path/to/HGSOC_cohort_data/", "wgd_status.txt"), header = TRUE)

sample <- sample %>% 
  add_column(WGD = NA) %>% 
  relocate(WGD, .before = CHROM)

if (startsWith(sample_name, "WGS")) {
  sample$WGD <- wgd %>% 
    dplyr::filter(grepl(paste0(sample_name, "-P"), Sample)) %>%
    pull(WholeGenomeDuplication)
} else {
  sample$WGD <- wgd %>% 
    dplyr::filter(grepl(sample_name, Sample)) %>%
    pull(WholeGenomeDuplication)
}

```



### Load and tidy copy number BED file

The BED file of PURPLE copy number calls is read in with *read.table*. The 'X' column i sremoved and the character string 'chr' added to the 'chromosome' column to facilitate easy joining of the files later.

```{r}

purple <- read.table(here::here("data", "Combined_purple_ids_tidy.bed"), header = TRUE, sep = "\t", row.names = NULL, stringsAsFactors = FALSE, fill = TRUE)

purple <- purple %>%
  select(-X)

purple$chromosome <- paste0("chr", purple$chromosome)

```

# Round copyNumber, minorAllelePloidy and majorAllelePloidy columns

As the PURPLE CN calls are model based, the 'copyNumber', 'majorAllelePloidy' and 'minorAllelePloidy' columns must be rounded before use.

```{r}

purple <- purple %>%
  mutate_at(vars(copyNumber, minorAllelePloidy, majorAllelePloidy), funs(round(.)))

```

# Filter for sample variants only

The CN calls are then filtered to single sample level.

```{r}

cn <- purple %>%
    dplyr::filter(grepl(sample_name, sample))

```

# Add key/driver genes list

The key/driver gene list is read in to allow for labelling of the MOBSTER model later in the script.

```{r}

gene_list <- read.table(here::here("path/to/HGSOC_cohort_data", "genelist.txt"), header = FALSE) %>% 
  pull(V1)

```



### Add copy number information

# Join sample and CN data

Next, I will use the *sqldf* function to join the tumour data to the copy number data. This is necessary as the variant positions in the sample file ('POS') need to be matched within a value range in the cn file ('start' - 'end'). This is not possible with the tidyverse *left_join* function, which I would normally use. I will also add a column for cellularity data and fill this with the cellularity estimate for the given tumour sample. Due to the naming conventions of the MDA data (i.e. samples beginning "WGS"), where in some file formats these sample names end in "-1" and in some formats end with "-P", I have written an if/else statement to account for this idiosyncracy in the data.

```{r}

sample <- sqldf("SELECT * FROM sample LEFT JOIN cn ON sample.CHROM = cn.chromosome AND sample.POS BETWEEN cn.start AND cn.end") %>%
  relocate(c(copyNumber, minorAllelePloidy, majorAllelePloidy), .after = POS)

```


### Calculate Cancer Cell Fraction

Now the data is tidy, I am able to calculate CCF. 

CCF = ((2 + p * (CN - 2)) / p) * f

Where: f = variant allele frequency (VAF)
       p = cellularity (or purity)
       NT = copy number
       
This column is named VAF rather than CCF as this is the required column name for MOBSTER fitting.

```{r}

sample <- sample %>%
  mutate(VAF = round(((((2 + CELLULARITY)*(copyNumber - 2)) / CELLULARITY)) * AF), 3) %>%
  relocate(c(VAF), .after = copyNumber)

```

# Format CCF column

The VAF column needs to be divided by 2 to represent a diploid heterozygous allele frequency.

```{r}

sample$VAF <- sample$VAF/2

```

### Load MAF file

Before fitting the MOBSTER moddel, I want to be able to label each position in the sample file with the correct gene name so that I can label the MOBSTER plot with the position of key/driver genes. To allow for this I need to join the Hugo (HGNC) Symbols from the sample MAF file to the sample VCF file, using columns vcf_pos/POS. (NB: 'vcf_pos' is a column in the MAF file which contains the position of the variant from the VCF file, 'POS').

```{r}

maf <- read.maf(maf = here::here("/path/to/HGSOC_cohort_data/mafs", maf_file))

```

The read.maf function separates variants by impact factor, generating a set of silent and non-silent variants. To be able to add gene labels to each variant from this data, I will need to join these two elements of the *read.maf* output, before I can join the gene names to the VCF dataset.

# Merge silent with non-silent variants from the MAF

```{r}

maf_tidy <- rbind(maf@data, maf@maf.silent)

```

# Join Hugo_Symbols to the VCF from the MAF file by variant position

I will now join the MAF file to the VCF file.

```{r}

sample <- left_join(sample, dplyr::select(maf_tidy, c(Hugo_Symbol, Chromosome, Gene, vcf_pos)), by = c("POS" = "vcf_pos", "CHROM" = "Chromosome")) %>% 
  relocate(c("Hugo_Symbol", "Gene"), .before = CHROM) %>%
  dplyr::rename(c(HGNC_NAME = Hugo_Symbol, GENE = Gene))

```

# Add boolean column to show if region is diploid and/or non-LOH

I now want to add columns to show whether a region is diploid or not, this will be required when filtering to the more robust diploid, non-LOH regions of the genome for the MOBSTER analysis.

```{r}

sample <- sample %>% 
  mutate(is_diploid = case_when(
    copyNumber == 2 ~ "TRUE",
    TRUE ~ FALSE)) %>% 
  relocate(is_diploid, .after = copyNumber)

```

I will do the same for tetraploid regions, which may be required for WGD samples.

```{r}

sample <- sample %>% 
  mutate(is_tetraploid = case_when(
    copyNumber == 4 ~ TRUE,
    TRUE ~ FALSE)) %>% 
  relocate(is_diploid, .after = is_diploid)

```

And to show regions where there is no loss of heterozygosity (LOH).

```{r}

sample <- sample %>% 
  mutate(is_nonLOH = case_when(
    majorAllelePloidy == minorAllelePloidy ~ TRUE,
    TRUE ~ FALSE)) %>% 
  relocate(is_nonLOH, .after = is_tetraploid)

```

Finally, I will move the 'major-' and 'minorAllelePloidy' columns next to the 'copyNumber' column for easy visualisation of the data in the file.

```{r}

sample <- sample %>%
  relocate(minorAllelePloidy, .after = copyNumber) %>%
  relocate(majorAllelePloidy, .after = copyNumber)

```

# Add column of boolean vectors to show if mutation is in a driver gene

For the plotting step, I need to add a column to show whether or not a mutation is in a driver gene. This will allow for labelling of the clusters in the plots. 

```{r}

sample <- sample %>% 
  mutate(driver_label = ifelse(HGNC_NAME %in% gene_list, HGNC_NAME, NA)) %>% 
  mutate(is_driver = if_else(HGNC_NAME %in% gene_list, TRUE, FALSE))

```



### MOBSTER 

# Filter to diploid/non-LOH regions and remove unnecessary columns to produce correct df format for MOBSTER

I will filter the sample to diploid/non-LOH regions for the fitting of the MOBSTER model to ensure the data is as robust as possible for this section of the analysis. 

```{r}

sample_small <- sample %>% 
  dplyr::filter(is_diploid == TRUE) %>% 
  dplyr::filter(is_nonLOH == TRUE)

```

In some samples, this filtering step still leaves some variants with VAF >1, these need to be removed before running MOBSTER as it deos not accept variants with a VAF >1.

```{r}

sample_small <- sample_small %>% 
  dplyr::filter(VAF < 1)

```

# Fit MOBSTER model

The sample can now be fitted with a MOBSTER model (NB: parallel = FALSE is specified as parallel runs cannot be reconciled in Eddie, which causes failure of the job script).

```{r}

fit <- mobster_fit(sample_small, parallel = FALSE)

```

The output is saved as an RDS file in Eddie so that it can be read in locally for plotting.

```{r}

#saveRDS(fit, file = here::here("output", paste0(sample_name, "_wgd", wgd_status, ".rds")))

```

# Identify number of clusters in sample

I will extract the number of clusters identified in the MOBSTER model and save this to a CSV file, so that the number of cluster4s for each sample is collated in one place. These can then later be added the VCF file for each sample.

```{r}

clusters <- Clusters(fit$best) %>% 
  distinct(cluster)

wgd_status <- sample[1,] %>% 
  pull(WGD)

ncluster <- data.frame(sample = sample_name, wgd_status = wgd_status, clusters = nrow(clusters) - 1)

```

```{r}

#write_csv(ncluster, here::here("output", "HGSOC_clusters.csv"), append = TRUE)

```

# Plot the MOBSTER model for the sample

```{r}

mobster_plot <- plot(fit$best)
  
```

# Save plot as PDF

```{r}

pdf(width = 16, height = 12, file = here::here("output", paste0(sample_name, "_mobster_plot.pdf")))

plot(fit$best)

dev.off()

```




